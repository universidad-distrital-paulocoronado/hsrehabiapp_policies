Privacy Policy for HSRehabiApp
Last updated: 2024-02-23
At Universidad Distrital Francisco José de Caldas, the privacy and security of our users are of paramount importance. HSRehabiApp ("we", "us", or "our") is committed to protecting the privacy of your information. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you use our HSRehabiApp application (the "Application").

Information Collection and Use
Camera Access
The Application uses the android.permission.CAMERA permission to access the device's camera for capturing photos or videos. The Application does not collect, store, or transmit any images or videos without your explicit consent. The images or videos captured are used only for professional feedback and are processed locally on your device.

How We Use Your Information
The information we collect is used in the following ways:

To provide and maintain our Application's functionality.
To improve user experience.
Data Sharing and Disclosure
We do not share or disclose your personal information with third parties except in the following cases:

With your consent.
For legal reasons, when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request.
To affiliated companies or third-party service providers who assist us in providing services or conducting business, under confidentiality agreements.
Data Security
We take reasonable measures to protect the information you provide from loss, theft, misuse, unauthorized access, disclosure, alteration, and destruction. However, no internet or email transmission is ever fully secure or error-free. Please keep this in mind when providing any information to us or using our services.

Children's Privacy
HSRehabiApp does not knowingly collect or solicit personal information from anyone under the age of 13 without parental consent. If you are under 13, please do not send any information about yourself to us, including your name, address, telephone number, or email address. In the event that we learn that we have collected personal information from a child under age 13 without verification of parental consent, we will delete that information as quickly as possible.

Changes to This Privacy Policy
We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. You are advised to review this Privacy Policy periodically for any changes.

Contact Us
If you have any questions about this Privacy Policy, please contact us at medicina@udistrital.edu.co.